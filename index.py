import sys
from PyQt4 import QtGui, QtCore


class Window(QtGui.QMainWindow):
    def __init__(self):
        super(Window, self).__init__()
        self.setGeometry(50, 50, 500, 300)
        self.setWindowTitle("Hello PyQt!!")
        self.setWindowIcon(QtGui.QIcon("python.png"))
        self.home()

    def home(self):
        btn = QtGui.QPushButton('Button', self)
        btn.clicked.connect(self.close_application)
        btn.setToolTip('This is a <b>QPushButton</b> widget')
        print btn.sizeHint()
        btn.resize(QtCore.QSize(80, 150))
        btn.move(50, 50)
        self.show()

    def close_application(self):
        choice = QtGui.QMessageBox.question(self, 'Exit' 'Are you sure?', QtGui.QMessageBox.Yes)

        if choice == QtGui.QMessageBox.Yes:
            sys.exit()
        else:
            pass


def run():
    app = QtGui.QApplication(sys.argv)
    GUI = Window()
    sys.exit(app.exec_())


run()
